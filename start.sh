kubectl create -f ./definitions/mongo-service.yml
kubectl create -f ./definitions/user-service.yml
kubectl create -f ./definitions/mysql-service.yml
kubectl create -f ./definitions/project-service.yml
kubectl create -f ./definitions/postgres-service.yml
kubectl create -f ./definitions/task-service.yml
kubectl create -f ./definitions/api-service.yml
kubectl create -f ./definitions/proxy-service.yml
